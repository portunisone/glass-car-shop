import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import DefaultLayout from "@/layouts/DefaultLayout";

createApp(App)
  .use(store)
  .use(router)
  .component("default-layout", DefaultLayout)

  .mount("#app");
