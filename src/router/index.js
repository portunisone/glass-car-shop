import { createRouter, createWebHistory } from "vue-router";

import store from "@/store";

const routes = [
  /**
   * Если неверный url кинет на 404
   */
  {
    path: "/:catchAll(.*)",
    name: "Notfound",
    component: () => import("../views/NotFoundView.vue"),
  },
  {
    path: "/",
    name: "HomeView",
    component: () => import("../views/HomeView.vue"),
    meta: {
      layout: "default-layout",
    },
  },
  {
    path: "/catalog",
    name: "CatalogView",
    component: () => import("../views/CatalogView.vue"),
    meta: {
      layout: "default-layout",
    },
  },
  {
    path: "/product/:id",
    name: "ProductView",
    component: () => import("../components/Catalog/ProductView.vue"),
    meta: {
      layout: "default-layout",
    },
  },
  {
    path: "/profile",
    name: "ProfileView",
    component: () => import("../views/ProfileView.vue"),
    meta: {
      layout: "default-layout",
      requiresAuth: true,
    },
  },
  {
    path: "/auth",
    name: "AuthComponent",
    component: () => import("../components/AuthComponent.vue"),
    meta: {
      requiresVisitor: true,
      layout: "default-layout",
    },
  },
  {
    path: "/register",
    name: "RegisterComponent",
    component: () => import("../components/RegisterComponent.vue"),
    meta: {
      layout: "default-layout",
      requiresVisitor: true,
    },
  },
  {
    path: "/cart",
    name: "CartList",
    component: () => import("../components/Cart/CartList.vue"),
    meta: {
      layout: "default-layout",
    },
  },
  {
    path: "/feedback",
    name: "feedBack",
    component: () => import("../components/Feedback/FeedBack.vue"),
    meta: {
      layout: "default-layout",
    },
  },
  {
    path: "/categories",
    name: "CategoryList",
    component: () => import("../components/Admin/Category/CategoryList.vue"),
    meta: {
      layout: "default-layout",
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters["moduleUser/loggedIn"]) {
      next({
        name: "AuthComponent",
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.requiresVisitor)) {
    if (store.getters["moduleUser/loggedIn"]) {
      next({
        name: "ProfileView",
      });
    } else {
      next();
    }
  } else {
    next();
  }
});
export default router;
