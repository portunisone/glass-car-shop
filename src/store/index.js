import { createStore } from "vuex";
import axios from "axios";
import { moduleUser } from "@/store/modules/moduleUser";
import { moduleProduct } from "@/store/modules/moduleProduct";
import { moduleCart } from "@/store/modules/moduleCart";
import { moduleOrder } from "@/store/modules/moduleOrder";
import { moduleCategory } from "@/store/modules/moduleCategory";
import { moduleFeedback } from "@/store/modules/moduleFeedback";

axios.defaults.baseURL = "http://glass-shop/api";
export default createStore({
  modules: {
    moduleUser,
    moduleProduct,
    moduleCart,
    moduleOrder,
    moduleCategory,
    moduleFeedback,
  },
});
