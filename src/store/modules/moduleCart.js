import axios from "axios";
import { moduleUser } from "@/store/modules/moduleUser";

export const moduleCart = {
  namespaced: true,
  state: () => ({
    cart: [],
  }),
  mutations: {
    UPDATE_CART(state, cart) {
      state.cart = cart;
    },
    REMOVE_CART(state) {
      state.cart = [];
    },
  },
  actions: {
    /**
     * Очищаем корзинку
     * @param commit
     * @returns {Promise<unknown>}
     * @constructor
     */
    REMOVE_CART({ commit }) {
      return new Promise((resolve) => {
        commit("REMOVE_CART");
        localStorage.removeItem("cart");
        resolve();
      });
    },
    /**
     * Добавляем товар в корзину
     * @param commit
     * @param state
     * @param dispatch
     * @param product - {} product
     * @constructor
     */
    ADD_TO_CART({ commit, state, dispatch }, product) {
      let cart = state.cart || [];
      product["quantity"] = 1;
      if (cart.find((it) => it.id === product.id) === undefined) {
        cart.push(product);
      }
      console.log("Добавил");
      dispatch("ADD_ITEM_LOCAL_STORAGE", cart);
    },
    /**
     * Добавляем в localstorage
     * @param commit
     * @param dispatch
     * @param payload - [] - cart
     * @constructor
     */
    ADD_ITEM_LOCAL_STORAGE({ commit, dispatch }, payload) {
      localStorage.setItem("cart", JSON.stringify(payload));
      dispatch("UPDATE_STATE");
    },
    /**
     * Удаляем из корзины
     * @param commit
     * @param state
     * @param index - id product
     * @constructor
     */
    DELETE_FROM_CART({ commit, state }, index) {
      let cart = state.cart;
      for (let i in cart) {
        if (cart[i].id === index) {
          cart.splice(i, 1);
        }
      }
      this.dispatch("ADD_ITEM_LOCAL_STORAGE", cart);
    },
    /**
     * Прибавляем количество единиц товара в корзине
     * @param commit
     * @param state
     * @param dispatch
     * @param payload - product
     * @constructor
     */
    INCREMENT_CART_ITEM({ commit, state, dispatch }, payload) {
      let cart = state.cart;
      cart = cart.map((item) => {
        if (item.id === payload.id && item.quantity < payload.count) {
          item.quantity++;
        }
        return { ...item };
      });
      dispatch("ADD_ITEM_LOCAL_STORAGE", cart);
    },
    /**
     * Уменьшаем количество единиц товара в корзине
     * @param commit
     * @param state
     * @param payload
     * @constructor
     */
    DECREMENT_CART_ITEM({ commit, state }, payload) {
      let cart = state.cart;
      cart = cart.map((item) => {
        if (item.id === payload.id) {
          if (item.quantity === 1) {
            return { ...item };
          } else {
            item.quantity--;
          }
        }
        return { ...item };
      });
      this.dispatch("ADD_ITEM_LOCAL_STORAGE", cart);
    },
    /**
     * Обновляем данные о корзине
     * @param commit
     * @constructor
     */
    UPDATE_STATE({ commit }) {
      let cart = window.localStorage.getItem("cart");
      commit("UPDATE_CART", JSON.parse(cart));
    },
  },
};
