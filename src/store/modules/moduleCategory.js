import axios from "axios";

export const moduleCategory = {
  namespaced: true,
  state: () => ({
    category: [],
  }),
  mutations: {
    SET_CATEGORY(state, category) {
      state.category = category;
    },
  },
  actions: {
    /**
     * Фильтрация товаров по категориям
     * @param id_category - id категории
     * @returns {Promise<AxiosResponse<any>>}
     * @constructor
     */
    async SET_CATEGORY({}, id_category) {
      return await axios.get(`/category/product/${id_category}`);
    },
    /**
     * Создаем категорию
     * @param state
     * @param params - название категории
     * @returns {Promise<void>}
     * @constructor
     */
    async CREATE_CATEGORY({ state }, params) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      await axios.post("/category", params).then((response) => {});
    },
    /**
     * Обновляем название категории
     * @param category
     * @returns {Promise<void>}
     * @constructor
     */
    async UPDATE_CATEGORY({}, category) {
      axios.defaults.headers.common["Authorization"] =
          "Bearer " + localStorage.getItem("access_token");
      await axios.patch(`category/${category.id}`, category);
    },
    /**
     * Удаление категории
     * @param id_category
     * @returns {Promise<void>}
     * @constructor
     */
    async DELETE_CATEGORY({}, id_category) {
      axios.defaults.headers.common["Authorization"] =
          "Bearer " + localStorage.getItem("access_token");
      await axios.delete(`/category/${id_category}`);
    },

    /**
     * Получаем все категории
     * @param commit
     * @returns {Promise<unknown>}
     * @constructor
     */
    GET_CATEGORY({ commit }) {
      return new Promise((resolve) => {
        axios.get("/category").then((response) => {
          commit("SET_CATEGORY", response.data);
          resolve(response.data.data);
        });
      });
    },
  },
};
