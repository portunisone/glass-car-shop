import axios from "axios";

export const moduleFeedback = {
  namespaced: true,
  actions: {
    /**
     * Создаем запрос на обратную связь
     * @param params - {} fio,phone
     * @returns {Promise<void>}
     * @constructor
     */
    async CREATE_FEEDBACK({}, params) {
      await axios.post("feedback", params);
    },
    /**
     * Получаем все запросы на обратную связь
     * @returns {Promise<AxiosResponse<any>>}
     * @constructor
     */
    async GET_FEEDBACK() {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");
      return await axios.get("feedback");
    },
    /**
     * Меняем статус запроса
     * @param state
     * @param status
     * @returns {Promise<void>}
     * @constructor
     */
    async CHANGE_STATUS_FEEDBACK({ state }, status) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");
      await axios.get(`/feedback/${status.url}`, status);
    },
  },
};
