import axios from "axios";

export const moduleOrder = {
  namespaced: true,
  actions: {
    /**
     * Меняем статус у заказа
     * @param state
     * @param status - {} status, url
     * @returns {Promise<void>}
     * @constructor
     */
    async CHANGE_STATUS_ORDER({ state }, status) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      await axios.post(`/change-status/${status.url}`, status);
    },
    /**
     * Получаем все заказы
     * @param state
     * @returns {Promise<AxiosResponse<any>>}
     * @constructor
     */
    async GET_ORDER({ state }) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      return await axios.get("/order");
    },
    /**
     * Оформляем заказ
     * @param state
     * @param order - [] - products
     * @returns {Promise<void>}
     * @constructor
     */
    async PLACE_ORDER({ state }, order) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");
      await axios.post("/order", order);
    },
  },
};
