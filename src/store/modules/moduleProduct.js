import axios from "axios";

export const moduleProduct = {
  namespaced: true,
  actions: {
    /**
     * Получаем все товары
     * @returns {Promise<AxiosResponse<any>>}
     * @constructor
     */
    async GET_PRODUCTS() {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      return await axios.get("/product");
    },
    /**
     * Получаем обределенный продукт
     * @param id_product - id продукта
     * @returns {Promise<AxiosResponse<any>>}
     * @constructor
     */
    async GET_PRODUCT({}, id_product) {
     return  await axios.get(`/product/${id_product}`)
    },
    /**
     * Создаем продукт
     * @param state
     * @param params - {} product
     * @returns {Promise<void>}
     * @constructor
     */
    async CREATE_PRODUCT({ state }, params) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      await axios.post("/product", params);
    },
    /**
     * Удаляем продукт
     * @param state
     * @param dispatch
     * @param id_product - id product
     * @returns {Promise<void>}
     * @constructor
     */
    async DELETE_PRODUCT({ state, dispatch }, id_product) {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");

      await axios.delete(`/product/${id_product}`).then(() => {});
    },
    /**
     * Обновляем продукт
     * @param product - {} product
     * @returns {Promise<void>}
     * @constructor
     */
    async UPDATE_PRODUCT({}, product) {
      console.log(product)
      await axios.patch(`/product/${product.id}`, product.product);
    },
  },
};
