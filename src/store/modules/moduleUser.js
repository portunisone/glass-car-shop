import axios from "axios";

export const moduleUser = {
  namespaced: true,
  state: () => ({
    token: localStorage.getItem("access_token") || null,
    user: {},
  }),
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, user) {
      state.user = user;
    },
    REMOVE_TOKEN(state) {
      state.token = null;
    },
  },
  actions: {
    /**
     * Получаем авторизированого пользователя
     * @param state
     * @param commit
     * @returns {Promise<void>}
     * @constructor
     */
    async GET_USER({ state, commit }) {
      axios.defaults.headers.common["Authorization"] = "Bearer " + state.token;
      if (state.token) {
        await axios.get("/profile").then((response) => {
          commit("SET_USER", response.data);
        });
      }
    },
    /**
     * Регистрируем пользователя
     * @param params - {} fio,login,password
     * @returns {Promise<void>}
     * @constructor
     */
    async REGISTER_USER({}, params) {
      await axios.post("/signup", params);
    },
    /**
     * Авторизируем пользователя
     * @param commit
     * @param params - {} login, password
     * @returns {Promise<void>}
     * @constructor
     */
    async AUTH_USER({ commit }, params) {
      await axios.post("/login", params).then((response) => {
        const token = response.data.access_token;
        localStorage.setItem("access_token", token);
        commit("SET_TOKEN", token);
      });
    },
    /**
     * Выводим пользователя из системы
     * @param commit
     * @param dispatch
     * @returns {Promise<void>}
     * @constructor
     */
    async LOGOUT_USER({ commit, dispatch }) {
      await axios
        .get("/logout")
        .then(() => {
          localStorage.removeItem("access_token");
          commit("REMOVE_TOKEN");
          commit("SET_USER", {})
        })
        .catch(() => {
          localStorage.removeItem("access_token");
          commit("REMOVE_TOKEN");
          commit("SET_USER", {})
        });
    },
  },
  getters: {
    loggedIn(state) {
      return state.token !== null;
    },
  },
};
